// Installation
//
// Install using go get or dep:
//
// * go get gitlab.com/intrusia/logfmt
//
// * dep ensure -add gitlab.com/intrusia/logfmt
package logfmt

import (
	"encoding/json"
	"regexp"
	"strings"
)

const rxLogFmt = `[ ^]?(\w+)=("(?:[^"\\]+|\\.)+"|[\S]+)`

var re = regexp.MustCompile(rxLogFmt)

// Map takes the log format line as input and
// returns the map of key:val (string:string) pair
func Map(lfLine string) map[string]string {
	lm := make(map[string]string)
	matches := re.FindAllStringSubmatch(lfLine, -1)
	for _, match := range matches {
		lm[match[1]] = strings.TrimRight(strings.TrimLeft(match[2], "\""), "\"")
	}
	return lm
}

// MapAll takes array of log format lines as input and
// returns the array of map by looping through Map call
func MapAll(lfLines []string) []map[string]string {
	var fullMap []map[string]string
	for _, lfLine := range lfLines {
		lm := Map(lfLine)
		fullMap = append(fullMap, lm)
	}
	return fullMap
}

// Marshal takes the log format line as input and
// returns the JSON
func Marshal(lfLine string) ([]byte, error) {
	lm := Map(lfLine)
	return json.Marshal(lm)
}

// MarshalAll takes array of log format lines as input and
// returns the equivalient JSON by looping through Marshal call
func MarshalAll(lfLines []string) ([]byte, error) {
	m := MapAll(lfLines)
	return json.Marshal(m)
}

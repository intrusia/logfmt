package logfmt_test

import (
	"logfmt"
	"reflect"
	"testing"
)

type test struct {
	input    string
	expected map[string]string
}

func TestMap(t *testing.T) {
	tests := []test{
		{
			`level=debug msg="Some debug message" client_ip=192.168.0.107 http_method=GET url="192.168.0.100/index"`,
			map[string]string{"level": "debug", "msg": "Some debug message", "client_ip": "192.168.0.107", "http_method": "GET", "url": "192.168.0.100/index"},
		}, {
			`level=unknown faulty msg=error message"`,
			map[string]string{"level": "unknown", "msg": "error"},
		}, {
			`level=unknown faulty msg="error message`,
			map[string]string{"level": "unknown", "msg": "error"},
		}, {
			`level=unknown faulty msg="error message field="faulty" field2="messy`,
			map[string]string{"level": "unknown", "msg": "error message field=", "field2": "messy"},
		}, {
			`level= msg=`,
			map[string]string{},
		}, {
			`level="te\"st" msg="Escaped\"string"`,
			map[string]string{"level": "te\\\"st", "msg": "Escaped\\\"string"},
		}, {
			`level="te'st" msg="Single''quote"`,
			map[string]string{"level": "te'st", "msg": "Single''quote"},
		}, {
			`level="te'st" msg="Single''quote"`,
			map[string]string{"level": "te'st", "msg": "Single''quote"},
		},
	}
	for i, tt := range tests {
		received := logfmt.Map(tt.input)
		eq := reflect.DeepEqual(tt.expected, received)
		if eq == false {
			t.Errorf("Invalid input in test case: %d", i+1)
			t.Errorf("Received: %+v, Expected: %+v", received, tt.expected)
		}
	}
}

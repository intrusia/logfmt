# logfmt
--
    import "logfmt"

### Installation

Install using go get or dep:

* go get gitlab.com/intrusia/logfmt

* dep ensure -add gitlab.com/intrusia/logfmt

## Usage

#### func  Map

```go
func Map(lfLine string) map[string]string
```
Map takes the log format line as input and returns the map of key:val
(string:string) pair

#### func  MapAll

```go
func MapAll(lfLines []string) []map[string]string
```
MapAll takes array of log format lines and returns the array of map by looping
through Map call

#### func  Marshal

```go
func Marshal(lfLine string) ([]byte, error)
```
Marshal takes the log format line as input and returns the JSON from the map
returned from Map call

#### func  MarshalAll

```go
func MarshalAll(lfLines []string) ([]byte, error)
```
MarshalAll takes array of log format lines as input and returns the equivalient
JSON by looping through Marshal call
